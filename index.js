/*
1. Опишіть, як можна створити новий HTML тег на сторінці.
Створити новий елемент із заданим тегом можна за допомогою метода document.createElement(tag).
Таож можна скористатися методом elem.insertAdjacentHTML, завдяки якому можна 
додавати будь-який HTML на сторінку.

2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі
варіанти цього параметра.
Перший параметр - це слово, що вказує, куди по відношенню до елементу робити вставку. Значення має бути одним із наступних:
`beforebegin' - вставити html безпосередньо перед elem,
`afterbegin' - вставити html на початок elem,
`beforeend' - вставити html в кінець elem,
`afterend' - вставити html безпосередньо після elem.

3. Як можна видалити елемент зі сторінки?
За допомогою методу .remove
*/

const arr2 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

    function getArray(arr, parent = document.body) {
    const ul = document.createElement("ul");
    parent.prepend(ul);
    let liElem;
    
    arr.forEach(element => {
        liElem = `
            <li>
                ${element}
            </li>`;
        ul.insertAdjacentHTML("beforeend", liElem)
        
    })
}
getArray(arr2)


// Очистити сторінку через 3 секунди. 
// Показувати таймер зворотного відліку (лише секунди) перед очищенням сторінки.

let p = document.createElement("p");
document.body.append(p);

let starter = 3;
let timer;

function countTime () {
    document.querySelector("p").innerHTML = starter;
    starter--;
    if (starter <= 0) {
        timer = setTimeout(() => {
                document.body.remove();
            }, 1000)
    } else {
        timer = setTimeout(countTime, 1000)
    }

}
countTime()